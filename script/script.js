'use strict'
// Блок категории товаров
let categoriesList = document.querySelector('#categories-list');
// Кнопка "купить" в карточке товара
let productCardButtons = document.querySelectorAll('.product-card__button');
// Форма заказа
let orderFormWrapper = document.querySelector('.order-form-wrapper');
//Кнопка "купить" в форме заказа
let orderFormBuyButton = document.querySelector('.order-form__buy-button');
// Кнопка "закрыть" в форме заказа
let orderFormCloseButton = document.querySelector('.order-form__close-button');
// Кнопка переключения темы
let themeButton = document.querySelector('#theme-button');

let body = document.querySelector('#body');
// Кнопка возврата наверх
let returnTopButton = document.querySelector('.return-top-button');
// Поле для ввода количества товара
let inputQuantity = document.querySelector('.input-quantity');
// Поле для комментария
let comment = document.querySelector('#comment');
// Кнопки выбора цвета товара
let radioBtns = document.querySelectorAll('.radio-btn');

const lang = navigator.language;

// Функция, которая преобразует дату добавления товара в человекочитаемый вид
const getDateInfo = () => {
  // Дата в карточке товара
  let dateAddedProducts = document.querySelectorAll('.product-card__date-added');
  // Создание нового объекта с текущей датой и временем
  let date = new Date();
  // День недели
  let dayName = date.toLocaleString(lang, { weekday: 'long' });
  // Число месяца
  let dayNumber = date.getDate();
  // Номер недели 
  let weekNumber = Math.ceil(dayNumber / 7);
  // Месяц
  let month = date.getMonth();
  // Название месяца
  let monthName = date.toLocaleString(lang, { month: 'long' });
  // Год
  let year = date.getFullYear();

  for (let i = 0; i < dateAddedProducts.length; i++) {
    if (!(monthName === 'март' || monthName === 'август')) {
      monthName = monthName.substring(0, monthName.length - 1) + 'я';
    } else {
      monthName = monthName + 'а';
    }
    dateAddedProducts[i].innerHTML = dayName[0].toUpperCase() + dayName.substring(1) + ',' + ' ' + weekNumber + ' неделя' + ' ' + monthName[0].toUpperCase() + monthName.substring(1) + ' ' + year + ' года';
  }
}

getDateInfo();

// Функция для валидации формы заказа
const formValidationCheck = () => {
  // Если введено целое число больше 0
  if (!Number.isNaN(Number(inputQuantity.value)) && (Number.isInteger(Number(inputQuantity.value))) && inputQuantity.value > 0) {
    // Проверяем выбран ли цвет
    for (let i = 0; i < radioBtns.length; i++) {
      if (radioBtns[i].checked) {
        alert('Товар добавлен в корзину!');
        orderFormWrapper.style.display = 'none';
        inputQuantity.value = '';
        comment.value = '';
        break;
      }
    }
  } else {
    alert('Укажите количество товара (целое число больше нуля)');
  }
}

// Функция для показа кнопки возврата наверх
const showReturnTopButton = () => {
  if (window.pageYOffset > 100) {
    returnTopButton.classList.remove('return-top-button_hidden');
  }
  else {
    returnTopButton.classList.add('return-top-button_hidden');
  }
}

// Валидация формы при нажатии кнопки "купить" в форме заказа
orderFormBuyButton.addEventListener('click', (event) => {
  event.preventDefault();
  formValidationCheck();
})

// При скролле появляется кнопка возрата наверх
window.addEventListener('scroll', () => {
  showReturnTopButton();
});

returnTopButton.addEventListener('click', () => {
  window.scrollTo(0, 0);
});

// Переключение темы 
themeButton.addEventListener('click', () => {
  body.classList.toggle('body_theme_light');
  categoriesList.classList.toggle('categories-list_theme_light');
})

// При нажатии кнопки "Закрыть" форма скрывается
orderFormCloseButton.addEventListener('click', (event) => {
  event.preventDefault();
  orderFormWrapper.style.display = 'none';
  inputQuantity.value = '';
  comment.value = '';
})

// Открытие формы заказа по клику на кнопку "купить" в карточке товара, цикл нужен для того чтобы событие применилось к каджой карточке
for (let productCardButton of productCardButtons) {
  productCardButton.addEventListener('click', () => {
    orderFormWrapper.style.display = 'block';
  })
}